const express = require('express')
const minimist = require('minimist')
const app = express()
const port = 5000 //Need to listen on 5000 for autodevops

var os = require('os')
var ifaceString = buildNetworkInterfaceString(os.networkInterfaces())

// For HTTP GET requests to root `/`, respond with basic `hello world` text
// plus some hostname and network interface info that is interesting for multi-pod deployments
app.get('/', function(req, res){

    res.send('Hello\r\n' + 'this server\'s hostname is: ' + os.hostname() + '\<br \/\>' +
        'this server\'s ip address(es) is/are: ' + ifaceString)


    // Try our best to log a meaningful requestor IP address to console
    var requestor_ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || 
    req.connection.remoteAddress || 
    req.socket.remoteAddress || 
    req.connection.socket.remoteAddress
    
    console.log('Request received from ' + requestor_ip)
})

app.listen(port, function () {
  // Once our express app is running, log basic information to console
  console.log(`Basic nodejs express app now listening on port ${port}!`)
  console.log(ifaceString)
})


// Build a string representative of the host's network interfaces
// argument: networkInterfaces - os.NetworkInterfaces()
// returns: ifaceString - a newline separate list of interface name and IP address
function buildNetworkInterfaceString(networkInterfaces) {
  var ifaceString = ''
  Object.keys(networkInterfaces).forEach(function (ifname) {
    var alias = 0
  
    networkInterfaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return
      }
  
      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        ifaceString += ifname + ':' + alias, iface.address + '\n'
      } else {
        // this interface has only one ipv4 adress
        ifaceString += ifname + ' ' + iface.address + '\n'
      }
      ++alias
    })
  })

  return ifaceString
}
